import * as React from 'react';
import { Route, RouteProps } from 'react-router-dom';
import { Dashboard } from '@app/Dashboard/Dashboard';
import { AppLayout } from '@app/AppLayout/AppLayout';
import { Support } from '@app/Support/Support';
import { GeneralSettings } from '@app/Settings/General/GeneralSettings';
import { ProfileSettings } from '@app/Settings/Profile/ProfileSettings';
import { NotFound } from '@app/NotFound/NotFound';
import { Login } from '@app/Login/Login';
import { Logout } from '@app/Logout/Logout';
import { RequireAuth } from './lib/Auth';

// let routeFocusTimer: number;
//
export type PFRouteProps = RouteProps & {
  label?: string;
  title?: string;
};

export type PFRoutePropsGroup = {
  label: string;
  children: PFRouteProps[];
};

export type AppRouteConfig = PFRouteProps | PFRoutePropsGroup;

const routes: AppRouteConfig[] = [
  {
    label: 'Dashboard',
    element: (
      <RequireAuth>
        <AppLayout title="Dashboard">
          <Dashboard />
        </AppLayout>
      </RequireAuth>
    ),
    path: '/',
    errorElement: <NotFound />,
  },
  {
    label: 'Support',
    element: (
      <RequireAuth>
        <AppLayout title="Support">
          <Support />
        </AppLayout>
      </RequireAuth>
    ),
    path: '/support',
  },
  {
    label: 'Settings',
    children: [
      {
        label: 'General',
        element: (
          <RequireAuth>
            <AppLayout title="PatternFly Seed | General Settings">
              <GeneralSettings />
            </AppLayout>
          </RequireAuth>
        ),
        path: '/settings/general',
      },
      {
        label: 'Profile',
        element: (
          <RequireAuth>
            <AppLayout title="PatternFly Seed | Profile Settings">
              <ProfileSettings />
            </AppLayout>
          </RequireAuth>
        ),
        path: '/settings/profile',
      },
    ],
  },
  {
    label: 'Login',
    element: (
      <AppLayout title="Login">
        <Login />
      </AppLayout>
    ),
    path: '/login',
  },
  {
    label: 'Logout',
    element: (
      <AppLayout title="Logout">
        <Logout />
      </AppLayout>
    ),
    path: '/logout',
  },
];

const AppRoutes = () => {
  return routes.map(({ ...props }, idx) => {
    return <Route {...props} key={idx} />;
  });
};

export { AppRoutes, routes };
