import * as React from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import useAuth from '@app/lib/Auth';
import { LoginFlow, UpdateLoginFlowBody, Session, SuccessfulNativeLogin } from '@ory/client';
import { UserAuthCard } from '@ory/elements';
import { EmptyState, PageSection, Spinner } from '@patternfly/react-core';

// import { EmptyState, EmptyStateIcon, PageSection, Title, Spinner } from '@patternfly/react-core';
import { sdk, sdkError } from '@app/lib/ory';
// import { Flow } from '@app/lib/ui';

const Login: React.FC = () => {
  const { login } = useAuth();
  const [flow, setFlow] = React.useState<LoginFlow>();
  const [searchParams, SetSearchParams] = useSearchParams();

  // The aal is set as a query parameter by your Ory project
  // aal1 is the default authentication level (Single-Factor)
  // aal2 is a query parameter that can be used to request Two-Factor authentication
  // https://www.ory.sh/docs/kratos/mfa/overview
  const aal2 = searchParams.get('aal2');

  // The login_challenge is a query parameter set by the Ory OAuth2 login flow
  // Switching between pages should keep the login_challenge in the URL so that the
  // OAuth flow can be completed upon completion of another flow (e.g. Registration).
  // https://www.ory.sh/docs/oauth2-oidc/custom-login-consent/flow
  const loginChallenge = searchParams.get('login_challenge');

  // The return_to is a query parameter is set by you when you would like to redirect
  // the user back to a specific URL after login is successful
  // In most cases it is not necessary to set a return_to if the UI business logic is
  // handled by the SPA.
  //
  // In OAuth flows this value might be ignored in favor of keeping the OAuth flow
  // intact between multiple flows (e.g. Login -> Recovery -> Settings -> OAuth2 Consent)
  // https://www.ory.sh/docs/oauth2-oidc/identity-provider-integration-settings
  const returnTo = searchParams.get('return_to');

  const navigate = useNavigate();

  // Get the flow based on the flowId in the URL (.e.g redirect to this page after flow initialized)
  const getFlow = React.useCallback(
    (flowId: string) =>
      sdk
        // the flow data contains the form fields, error messages and csrf token
        .getLoginFlow({ id: flowId })
        .then(({ data: flow }) => setFlow(flow))
        .catch(sdkErrorHandler),
    [],
  );

  // initialize the sdkError for generic handling of errors
  const sdkErrorHandler = sdkError(getFlow, setFlow, '/login', true);

  // Create a new login flow
  const createFlow = () => {
    sdk
      .createBrowserLoginFlow({
        refresh: true,
        aal: aal2 ? 'aal2' : 'aal1',
        ...(loginChallenge && { loginChallenge: loginChallenge }),
        ...(returnTo && { returnTo: returnTo }),
      })
      // flow contains the form fields and csrf token
      .then(({ data: flow }) => {
        // Update URI query params to include flow id
        SetSearchParams({ ['flow']: flow.id });
        // Set the flow data
        console.log('flow', flow);
        setFlow(flow);
      })
      .catch(sdkErrorHandler);
  };

  // submit the login form data to Ory
  const submitFlow = (body: UpdateLoginFlowBody) => {
    // something unexpected went wrong and the flow was not set
    if (!flow) return navigate('/login', { replace: true });

    // we submit the flow to Ory with the form data
    sdk
      .updateLoginFlow({ flow: flow.id, updateLoginFlowBody: body })
      .then(({ data }) => {
        const s = data as SuccessfulNativeLogin;
        console.log('session', s.session);
        // we successfully submitted the login flow, so lets redirect to the dashboard
        login(s.session);
        navigate('/', { replace: true });
      })
      .catch(sdkErrorHandler);
  };

  React.useEffect(() => {
    // we might redirect to this page after the flow is initialized, so we check for the flowId in the URL
    const flowId = searchParams.get('flow');
    // the flow already exists
    if (flowId) {
      getFlow(flowId).catch(createFlow); // if for some reason the flow has expired, we need to get a new one
      return;
    }

    // we assume there was no flow, so we create a new one
    createFlow();
  }, []);

  return flow ? (
    <PageSection>
      <UserAuthCard
        flow={flow}
        flowType={'login'}
        additionalProps={{
          forgotPasswordURL: '/recovery',
          signupURL: '/signup',
        }}
        title={'Login to Deterlab'}
        includeScripts={true}
        onSubmit={({ body }) => submitFlow(body as UpdateLoginFlowBody)}
      />
    </PageSection>
  ) : (
    <PageSection>
      <EmptyState>
        <Spinner />
      </EmptyState>
    </PageSection>
  );
};

export { Login };
