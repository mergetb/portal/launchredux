import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import useAuth from '@app/lib/Auth';
import { Button, PageSection, Title } from '@patternfly/react-core';
import { sdk, sdkError } from '@app/lib/ory';

const Logout: React.FC = () => {
  const { session } = useAuth();
  const [logoutUrl, setLogoutUrl] = React.useState<string>();
  const navigate = useNavigate();
  const sdkErrorHandler = sdkError(undefined, undefined, '/login');

  const createLogoutFlow = () => {
    // here we create a new logout URL which we can use to log the user out
    sdk
      .createBrowserLogoutFlow(undefined, {
        params: {
          return_url: '/',
        },
      })
      .then(({ data }) => setLogoutUrl(data.logout_url))
      .catch(sdkErrorHandler);
  };

  React.useEffect(() => {
    if (session) {
      createLogoutFlow();
    }
  }, [session]);

  return (
    <PageSection>
      <Title headingLevel="h1" size="lg">
        Logout!!
      </Title>
      <a href={logoutUrl}>Logout</a> or go to your settings page here: <a href="/settings">Settings</a>
    </PageSection>
  );
};

export { Logout };
