import * as React from 'react';
import '@patternfly/react-core/dist/styles/base.css';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import { routes } from '@app/routes';
import '@app/app.css';
import { AuthProvider } from './lib/Auth';
import { CustomTranslations, CustomLanguageFormats, IntlProvider, ThemeProvider, locales } from '@ory/elements';

// required styles for Ory Elements
import '@ory/elements/style.css';

// NOTE: The routes are an expanded RouteProp type, so TS will complain.
const router = createBrowserRouter(routes);

const customTranslations: CustomLanguageFormats = {
  en: {
    ...locales.en,
    'login.title': 'Login',
    'identities.messages.1070004': 'Username',
  },
  nl: {
    ...locales.nl,
    'login.title': 'Inloggen',
    'identities.messages.1070004': 'Username',
  },
  af: {
    // merging English since no default Afrikaans translations are available
    ...locales.en,
    'login.title': 'Meld aan',
    'identities.messages.1070004': 'E-posadres',
  },
};

const App: React.FunctionComponent = () => (
  <React.StrictMode>
    <AuthProvider>
      <ThemeProvider themeOverrides={{}}>
        <IntlProvider<CustomTranslations> locale="en" defaultLocale="en" customTranslations={customTranslations}>
          <RouterProvider router={router} />
        </IntlProvider>
      </ThemeProvider>
    </AuthProvider>
  </React.StrictMode>
);

export default App;
