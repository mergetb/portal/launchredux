import { Session } from '@ory/client';
import * as React from 'react';
import { Navigate, useLocation } from 'react-router-dom';

export const AuthContext = React.createContext<AuthContextType | null>(null);

export type AuthContextType = {
  authed: boolean;
  login: (session: Session) => Promise<void>;
  logout: () => Promise<void>;
  session?: Session;
};

export const AuthProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [authed, setAuthed] = React.useState(false);
  const [session, setSession] = React.useState<Session>();

  const login = (session: Session): Promise<void> => {
    return new Promise((result): void => {
      setAuthed(true);
      setSession(session);
      result();
    });
  };

  const logout = (): Promise<void> => {
    return new Promise((result): void => {
      setAuthed(false);
      result();
    });
  };

  return <AuthContext.Provider value={{ authed, login, logout, session }}>{children}</AuthContext.Provider>;
};

export const RequireAuth: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const { authed } = React.useContext(AuthContext) as AuthContextType;
  const location = useLocation();

  return authed === true ? children : <Navigate to="/login" replace state={{ path: location.pathname }} />;
};

export default function useAuth(): AuthContextType {
  return React.useContext(AuthContext) as AuthContextType;
}
