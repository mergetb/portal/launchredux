import * as React from 'react';
import { PageSection, Title } from '@patternfly/react-core';
import useAuth from '@app/lib/Auth';

const Dashboard: React.FunctionComponent = () => {
  const { session } = useAuth();

  return (
    <PageSection>
      <Title headingLevel="h1" size="lg">
        Dashboard Page Title!
      </Title>
      {session && <>logged in as {session?.identity?.traits.username}</>}
    </PageSection>
  );
};

export { Dashboard };
